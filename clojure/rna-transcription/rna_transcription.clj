(ns rna-transcription)

(defn- get-with-default-fn [fn map key]
  (if (contains? map key) (get map key) (fn)))

(defn to-rna [sentence]
  (let [dna-map {\C \G \G \C \A \U \T \A}
        default-fn #(assert false "Letter not found!")]
    (apply str (map #(get-with-default-fn default-fn dna-map %1) sentence))))
