(ns anagram
  (require [clojure.string :as str]))

(defn- lower [s]
  (str/join (map #(Character/toLowerCase %) s)))

(defn- sort-string [s]
  (str/join (sort s)))

(defn anagrams-for [word words]
  (filter #(and
             (not (= (lower word) (lower %1)))
             (= (sort-string (lower word)) (sort-string (lower %1))))
          words))
