(ns bob
  (:require [clojure.string :as string]))


(defn- letters [word]
  (apply str (filter #(Character/isLetter %) word)))

(defn- uppercase? [word]
  (every? #(Character/isUpperCase %) (letters word)))

(defn- silent? [sentence]
  (string/blank? sentence))

(defn- question? [sentence]
  (= \? (last sentence)))

(defn- screaming? [sentence]
  (let [alpha (letters sentence)]
    (and
      (uppercase? alpha)
      (not (empty? alpha)))))

(defn response-for [sentence]
  (cond
        (silent? sentence) "Fine. Be that way!"
        (screaming? sentence) "Woah, chill out!"
        (question? sentence) "Sure."
        :else "Whatever."))
