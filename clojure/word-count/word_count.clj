(ns word-count)

(defn- map-with [func default map key]
  (assoc map key (func (get map key default))))

(defn word-count [sentence]
  (reduce (partial map-with inc 0) {}
          (re-seq #"\w+" (.toLowerCase sentence))))
