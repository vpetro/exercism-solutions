class PhoneNumber(phoneNumber: String) {
  val number = phoneNumber.filter(_.isDigit) match {
    case s if s.length == 11 && s.head == '1' => s.tail
    case s if s.length == 10 => s
    case _ => "0000000000"
  }

  lazy val (areaCode, n) = number.splitAt(3)

  override def toString() = {
    val (prefix, suffix) = n.splitAt(3)
    s"($areaCode) $prefix-$suffix"
  }
}
