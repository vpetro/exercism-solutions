case class Year(year: Integer) {
  private def isDivisibleBy(a: Integer, b: Integer) = a % b == 0

  def isLeap() = {
    (isDivisibleBy(year, 4) && (!isDivisibleBy(year, 100) || isDivisibleBy(year, 400)))
  }
}
