class Dna {
  private val transcription = Map(
    'C' -> 'G',
    'G' -> 'C',
    'A' -> 'U',
    'T' -> 'A'
  )

  def toRna(letters: String): String = letters.map(transcription)
}

object Dna {
  def apply() = new Dna()
}
