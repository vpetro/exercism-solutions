import java.util.Calendar
import java.util.GregorianCalendar

case class Meetup(month: Int, year: Int) {
  val start = new GregorianCalendar(year, month - 1, 1)
  val teenthStart = new GregorianCalendar(year, month - 1, 13)
  val nextMonth = new GregorianCalendar(year, month, 1)

  def teenth(meetupDay: Int) = teenthStart.addDays(teenthStart.calculateDaysUntil(meetupDay))

  def last(meetupDay: Int) = nextMonth.addDays(-7).addDays(nextMonth.calculateDaysUntil(meetupDay))

  def first(meetupDay: Int) = start.addDays(start.calculateDaysUntil(meetupDay))
  def second(meetupDay: Int) = start.addDays(7 + start.calculateDaysUntil(meetupDay))
  def third(meetupDay: Int) = start.addDays(7 * 2 + start.calculateDaysUntil(meetupDay))
  def fourth(meetupDay: Int) = start.addDays(7 * 3 + start.calculateDaysUntil(meetupDay))

  implicit class ImmutableCalendar(calendar: Calendar) {
    def dayOfWeek(): Int = calendar.get(Calendar.DAY_OF_WEEK)
    def calculateDaysUntil(weekday: Int) = (Meetup.Sat - dayOfWeek + weekday) % 7
    def addDays(numDays: Int) = updateWith(_.add(Calendar.DAY_OF_YEAR, numDays))
    def copy: Calendar = calendar.clone.asInstanceOf[Calendar]
    def updateWith(func: Calendar => Unit) = {
      val _c = calendar.copy
      func(_c)
      _c
    }
  }

}

object Meetup {
  val Mon = Calendar.MONDAY
  val Tue = Calendar.TUESDAY
  val Wed = Calendar.WEDNESDAY
  val Thu = Calendar.THURSDAY
  val Fri = Calendar.FRIDAY
  val Sat = Calendar.SATURDAY
  val Sun = Calendar.SUNDAY
}


