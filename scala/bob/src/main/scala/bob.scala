class Bob {
  private def isSilent(sentence: String) : Boolean = {
    sentence.matches("\\s*")
  }

  private def isScreaming(sentence: String) : Boolean = {
    val letters = sentence.filter(_.isLetter)
    if (letters.isEmpty) {
      false
    } else {
      letters.forall(_.isUpper)
    }
  }

  private def isQuestion(sentence: String) : Boolean = sentence.endsWith("?")

  def hey(sentence: String) : String = sentence match {
    case _ if isSilent(sentence) => "Fine. Be that way!"
    case _ if isScreaming(sentence) => "Woah, chill out!"
    case _ if isQuestion(sentence) => "Sure."
    case _ => "Whatever."
    }
}


