import scala.util.Random

class Robot {

  private def createName() = {
    val r = (new Random).alphanumeric
    r.filter(_.isLetter).take(2).mkString + r.filter(_.isDigit).take(3).mkString
  }

  private var _name = createName()

  def name = _name
  def reset() = _name = createName
}
