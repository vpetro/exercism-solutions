class Accumulate {
  def accumulate[T, U](f: T => U, xs: Seq[T]) = xs.foldLeft(List.empty[U])((y, x) => y :+ f(x))
}
