object Grains {
  def square(num: Int): BigInt = BigInt(2).pow(num -1)

  def total: BigInt = square(65) - 1
}
