object Sublist {
  case object Equal
  case object Sublist
  case object Superlist
  case object Unequal
}

class Sublist {

  def sublist[T](xs: List[T], ys: List[T]) = {
    if (xs.equals(ys)) Sublist.Equal
    else if (ys.containsSlice(xs)) Sublist.Sublist
    else if (xs.containsSlice(ys)) Sublist.Superlist
    else Sublist.Unequal
  }
}
