class DNA(strand: String) {
  private val nucleotides = Map('A' -> 0, 'T' -> 0, 'C' -> 0, 'G' -> 0)

  // terrible validation
  if (!strand.forall(nucleotides.contains(_))) throw new IllegalArgumentException()

  def count(toCount: Char) = toCount match {
    case _ if nucleotides.contains(toCount) => strand.count(_ == toCount)
    case _ => throw new IllegalArgumentException()
  }

  lazy val nucleotideCounts = nucleotides ++ strand.groupBy(identity).mapValues(_.length)
}
