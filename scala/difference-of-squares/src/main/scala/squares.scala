import scala.math.{ pow }

class Squares {
  def squareOfSums(x: Int): Int = pow(0.to(x).sum, 2).toInt
  def sumOfSquares(x: Int): Int = 0.to(x).map(pow(_, 2)).sum.toInt
  def difference(x: Int): Int = squareOfSums(x) - sumOfSquares(x)
}

object Squares {
  def apply() = new Squares
}
