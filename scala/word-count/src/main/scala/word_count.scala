class Phrase(word: String) {
  def wordCount(): Map[String, Int] = {
    val cleanString = word.toLowerCase.split("[^a-z0-9']").filter(_.nonEmpty)
    cleanString.foldLeft(Map[String, Int]()) {
      case (a, s) => a.updated(s, a.getOrElse(s, 0) + 1)
    }
  }
}
