import scala.math.abs

object Hamming {
  def compute(a: String, b: String): Integer = a.zip(b).count({case (x, y) => x != y})
}
