class Anagram(word: String) {

  def matches(words: Seq[String]): Seq[String] = {
    words.filter(s => isAnagram(word.toLowerCase, s.toLowerCase))
  }

  private def isAnagram(word: String, other: String): Boolean = {
    word != other && word.sorted == other.sorted
  }
}
