import java.util.{ GregorianCalendar, Calendar }

case class Gigasecond(calendar: GregorianCalendar) {
  val c = calendar.clone.asInstanceOf[GregorianCalendar]
  c.add(Calendar.SECOND, 1000000000)
  c.set(Calendar.SECOND, 0)
  c.set(Calendar.MINUTE, 0)
  c.set(Calendar.HOUR, 0)
  val date = c
}
