class Scrabble {
  private val letters = List(
    (1, "AEIOULNRST"),
    (2, "DG"),
    (3, "BCMP"),
    (4, "FHVWY"),
    (5, "K"),
    (8, "JX"),
    (10, "QZ"))
  private val scoreTable = letters.flatMap(t => t._2.map(c => (c.toLower, t._1))).toMap
  def scoreLetter(letter: Char): Int = scoreTable.getOrElse(letter.toLower, 0)
  def scoreWord(word: String): Int = word.map(scoreLetter).sum
}
