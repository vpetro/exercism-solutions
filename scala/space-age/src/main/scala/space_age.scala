import scala.math.pow

case class SpaceAge(val seconds: Long) {
  lazy val onEarth = Earth(seconds).age
  lazy val onMercury = Mercury(seconds).age
  lazy val onJupiter =  Jupiter(seconds).age
  lazy val onVenus = Venus(seconds).age
  lazy val onMars = Mars(seconds).age
  lazy val onSaturn = Saturn(seconds).age
  lazy val onUranus = Uranus(seconds).age
  lazy val onNeptune = Neptune(seconds).age
}

abstract class Planet {
  val seconds: Long
  val factor: Double
  lazy val age: Double = convert(factor)

  private def convert(factor: Double): Double = {
    val secondsInYear = (365.25 * 24 * 60 * 60) 
    ((seconds / secondsInYear) / factor).roundTo(2)
  }

  implicit class ExtendedDouble(n: Double) {
    def roundTo(x: Int) = {
      val z = pow(10, x)
      (n * z).round / z.toDouble
    }
  }
}

case class Earth(val seconds: Long) extends Planet { val factor = 1.0 }
case class Mercury(val seconds: Long) extends Planet { val factor = 0.2408467}
case class Jupiter(val seconds: Long) extends Planet { val factor = 11.862615}
case class Venus(val seconds: Long) extends Planet { val factor = 0.61519726}
case class Mars(val seconds: Long) extends Planet { val factor = 1.8808158}
case class Saturn(val seconds: Long) extends Planet { val factor = 29.447498}
case class Uranus(val seconds: Long) extends Planet { val factor = 84.016846}
case class Neptune(val seconds: Long) extends Planet { val factor = 164.79132}
