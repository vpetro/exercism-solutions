import scala.collection.immutable.SortedMap

class School() {
  private var _db: Map[Int, List[String]] = Map[Int, List[String]]()

  def db: Map[Int, List[String]] = _db

  def add(name: String, grade: Int):Unit = {
    this.synchronized {
      _db = db.updated(grade, db.getOrElse(grade, List[String]()) :+ name)
    }
  }

  def grade(g: Int): List[String] = db.getOrElse(g, List[String]())

  def sorted: SortedMap[Int, List[String]] = {
    SortedMap[Int, List[String]](db.toArray.map(t => (t._1, t._2.sorted)):_*)
  }
}
