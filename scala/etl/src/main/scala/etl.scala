object ETL {
  def transform(input: Map[Int, Seq[String]]): Map[String, Int] = {
    input.flatMap { case (k, v) => v.map(l => (l.toLowerCase, k))}
  }
}

