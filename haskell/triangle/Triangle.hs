module Triangle(TriangleType(..), triangleType) where

data TriangleType = Equilateral
                  | Isosceles
                  | Scalene
                  | Illogical deriving (Show, Eq)

triangleType :: Int -> Int -> Int -> TriangleType
triangleType a b c
  | isIllogical = Illogical
  | isEqualateral = Equilateral
  | isIsosceles = Isosceles
  | otherwise = Scalene
  where
    isIllogical = not $ and [a /= 0, b /= 0, c /= 0, a + b > c, b + c > a, c + a > b]
    isEqualateral = a == b && b == c
    isIsosceles = or [a == b, b == c, a == c]
