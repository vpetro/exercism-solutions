module LinkedList (
  List,
  new,
  fromList,
  toList,
  nil,
  next,
  reverseLinkedList,
  datum,
  isNil
) where

data List a = Cons a (List a) | Nil deriving (Show, Eq)

new :: a -> List a -> List a
new x l = Cons x l

fromList :: [a] -> List a
fromList [] = Nil
fromList (x:xs) = Cons x $ fromList xs

toList :: List a -> [a]
toList Nil = []
toList (Cons x y) = x : toList y

nil :: List a
nil = Nil

isNil :: List a -> Bool
isNil Nil = True
isNil _ = False

datum :: List a -> a
datum Nil = error "Empty list"
datum (Cons x _) = x

reverseLinkedList :: List a -> List a
reverseLinkedList l = rev Nil l
  where rev a (Cons x y) = rev (Cons x a) y
        rev a Nil = a

next :: List a -> List a
next Nil = Nil
next (Cons _ y) = y
