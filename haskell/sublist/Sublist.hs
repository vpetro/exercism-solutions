module Sublist (
  sublist, Sublist(..)
) where

import Data.List (isInfixOf)

data Sublist = Sublist | Equal | Unequal | Superlist deriving (Eq, Show)

sublist :: Ord a => [a] -> [a] -> Sublist
sublist a b
  | a == b = Equal
  | a `isInfixOf` b = Sublist
  | b `isInfixOf` a = Superlist
  | otherwise = Unequal

