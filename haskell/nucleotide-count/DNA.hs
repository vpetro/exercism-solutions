module DNA (count, nucleotideCounts) where

import Data.Map.Strict (Map, fromList, fromListWith, union)

count :: Char -> String -> Int
count needle "" = 0
count needle haystack
  | needle `elem` "GACTU" = length $ filter (== needle) haystack
  | otherwise = error $ "invalid nucleotide " ++ show needle

nucleotideCounts :: String -> Map Char Int
nucleotideCounts cs = union counted start
  where
    start = fromList [('A',0),('C',0),('G',0),('T',0)]
    counted = fromListWith (+) [(c, 1) | c <- cs]
