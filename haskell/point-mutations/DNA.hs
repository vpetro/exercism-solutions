module DNA (hammingDistance) where

hammingDistance :: String -> String -> Int
hammingDistance a b = sum $ zipWith (\x y -> fromEnum $ x /= y) a b
