module Beer (sing, verse) where

sing :: Int -> Int -> String
sing startBottles endBottles = unlines song
    where
        song = map verse bottles
        bottles = [startBottles, startBottles - 1 .. endBottles]

verse :: Int -> String
verse 0 = "No more bottles of beer on the wall, no more bottles of beer.\n" ++ nextVerse 0
verse 1 = "1 bottle of beer on the wall, 1 bottle of beer.\n" ++ nextVerse 1
verse numBottles = show numBottles ++ " bottles of beer on the wall, " ++ show numBottles ++ " bottles of beer.\n" ++ nextVerse numBottles

nextVerse :: Int -> String
nextVerse 0 = "Go to the store and buy some more, 99 bottles of beer on the wall.\n"
nextVerse 1 = "Take it down and pass it around, no more bottles of beer on the wall.\n"
nextVerse 2 = "Take one down and pass it around, 1 bottle of beer on the wall.\n"
nextVerse numBottles = "Take one down and pass it around, " ++ (show $ numBottles - 1) ++ " bottles of beer on the wall.\n"
