module SpaceAge (Planet(..), ageOn) where

data Planet = Earth | Mercury | Venus | Mars | Jupiter | Saturn | Uranus | Neptune

ageOn :: Planet -> Integer -> Float
ageOn planet seconds = fromIntegral seconds / (31557600.0 * secondsPerYear)
  where
    secondsPerYear = case planet of
      Earth -> 1.0
      Mercury -> 0.2408467
      Venus -> 0.61519726
      Mars -> 1.8808158
      Jupiter -> 11.86261
      Saturn -> 29.447498
      Uranus -> 84.016846
      Neptune -> 164.79132
