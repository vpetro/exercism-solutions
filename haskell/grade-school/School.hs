module School (School(), empty, add, sorted, grade) where

import Data.List as L (sort)
import Data.Map.Strict (Map, insertWith, toAscList, findWithDefault, fromList)

type Grade = Int
type Name = String
newtype School = School { unSchool :: Map Int [String] }

add :: Grade -> Name -> School -> School
add cls name school = School $ insertWith (++) cls [name] $ unSchool school

empty :: School
empty = School $ fromList []

sorted :: School -> [(Grade, [Name])]
sorted school = [(c, L.sort ns) | (c, ns) <- toAscList $ unSchool school]

grade :: Grade -> School -> [Name]
grade g = findWithDefault [] g . unSchool
