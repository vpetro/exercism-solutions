module Grains (square, total) where

square :: Integer -> Integer
square s = 2 ^ (s - 1)

total :: Integer
total = square 65 - 1

