module ETL (transform) where

import Data.Char (toLower)
import Data.Map.Strict (Map, toList, fromList)


transform :: Map Int [String] -> Map String Int
transform = fromList . concatMap convert . toList
  where convert (score, letters) = zip [map toLower l | l <- letters] $ repeat score

