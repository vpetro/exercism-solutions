module Phone (areaCode, number, prettyPrint) where

import Data.List (intercalate)
import Data.Char (isDigit)

number :: String -> String
number ns
  | numDigits == 11 && head digits == '1' = tail digits
  | numDigits == 10 = digits
  | otherwise = "0000000000"
  where digits = filter isDigit ns
        numDigits = length digits

areaCode :: String -> String
areaCode = take 3 . number

prettyPrint :: String -> String
prettyPrint ns = "(" ++ areaCode digits ++ ") " ++ intercalate "-" [prefix, rest]
  where
    digits = number ns
    (prefix, rest) = splitAt 3 $ drop 3 digits
