module Bob (responseFor) where

import Data.Char (isAlpha, isSpace, isUpper)

isScreaming :: String -> Bool
isScreaming s =  isNonEmpty && onlyUpper
  where alpha = filter isAlpha s
        isNonEmpty = not $ null alpha
        onlyUpper = all isUpper alpha

isQuestion :: String -> Bool
isQuestion = ('?' ==) . last

isSilent :: String -> Bool
isSilent = all isSpace

responseFor :: String -> String
responseFor s
  | isSilent s = "Fine. Be that way!"
  | isScreaming s = "Woah, chill out!"
  | isQuestion s = "Sure."
  | otherwise = "Whatever."
