module SumOfMultiples (sumOfMultiples, sumOfMultiplesDefault) where

sumOfMultiplesDefault :: Integer -> Integer
sumOfMultiplesDefault = sumOfMultiples [3, 5]

sumOfMultiples :: [Integer] -> Integer -> Integer
sumOfMultiples mults n = sum [if any ((== 0) . mod a) mults then a else 0 | a <- [1..n-1]]
