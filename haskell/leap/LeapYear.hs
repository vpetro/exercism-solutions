module LeapYear (isLeapYear) where


isLeapYear :: Int -> Bool
isLeapYear year = byFour && (not byHundred || byFourHundred)
  where
    divBy y num = mod y num == 0
    byFour = divBy year 4
    byHundred = divBy year 100
    byFourHundred = divBy year 400
