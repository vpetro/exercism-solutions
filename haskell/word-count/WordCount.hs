module WordCount (wordCount) where

import Data.Map (Map, fromList)
import Data.List (head, group, sort)
import Data.List.Split (wordsBy)
import Data.Char (toLower, isAlphaNum)

wordCount :: String -> Map String Int
wordCount s = fromList $ map groupSize $ groupWords $ tokenize s
    where
        groupSize ws = (head ws, length ws)
        groupWords = group . sort
        tokenize = wordsBy (not . isAlphaNum) . map toLower
