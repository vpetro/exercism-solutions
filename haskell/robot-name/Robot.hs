module Robot (robotName, mkRobot, resetName) where

import Data.IORef
import Control.Monad (liftM)
import System.Random (newStdGen, randomRs)

newtype Robot = Robot (IORef String) 

robotName :: Robot -> IO String
robotName (Robot ref) = readIORef ref

mkRobot :: IO Robot
mkRobot = liftM Robot (generateName >>= newIORef)

resetName :: Robot -> IO ()
resetName (Robot ref) = generateName >>= writeIORef ref


generateName :: IO String
generateName = do
  g <- newStdGen
  let a = take 2 (randomRs ('A', 'Z') g)
  let b = take 3 (randomRs ('0', '9') g)
  return $ a ++ b
