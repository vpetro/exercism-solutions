module Meetup (Schedule(..), Weekday(..), meetupDay) where

import Data.Time.Calendar (
  Day, fromGregorian,
  addGregorianMonthsClip,
  addDays)
import Data.Time.Calendar.WeekDate (toWeekDate)

data Schedule = Teenth | Last | First | Second | Third | Fourth deriving (Show, Eq, Enum)

data Weekday = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday deriving (Show, Eq, Enum)

type Year = Integer
type Month = Int

third :: (t, t1, t2) -> t2
third (_, _, x) = x

{--
{-- this is the solution that uses lists --}
meetupDay :: Schedule -> Weekday -> Year -> Month -> Day
meetupDay schedule weekday year month = date
  where date = case schedule of
                Teenth -> getDate id 13 19
                Last -> getDate id 21 monthLength
                _ -> getDate tf 1 monthLength
        f = (== fromEnum weekday + 1) . third . toWeekDate
        tf = take (fromEnum schedule -1)
        monthLength = gregorianMonthLength year month
        days start end = [fromGregorian year month d | d <- [start..end]]
        getDate tf' start end = last $ tf' $ filter f $ days start end
--}


addWeeks :: Int -> Day -> Day
addWeeks = addDays . (7 *) . fromIntegral

monthStart :: Integer -> Int -> Day
monthStart year month = fromGregorian year month 1

nextMonth :: Day -> Day
nextMonth = addGregorianMonthsClip 1

meetupDay' :: Weekday -> Day -> Day
meetupDay' weekday day = addDays (fromIntegral x) day
  where x | enumWeekday >= curWeekdayNumber = diff
          | otherwise = 7 + diff
        enumWeekday = fromEnum weekday + 1
        curWeekdayNumber = third $ toWeekDate day
        diff = enumWeekday - curWeekdayNumber

meetupDay :: Schedule -> Weekday -> Year -> Month -> Day
meetupDay schedule weekday year month = result
  where result = case schedule of
                Last -> meetupDay' weekday $ addWeeks (-1) $ nextMonth ms
                Teenth -> meetupDay' weekday $ addDays 12 ms
                _ -> meetupDay' weekday $ addWeeks (fromEnum schedule - 2) ms
        ms = monthStart year month
