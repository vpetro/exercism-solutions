module Gigasecond (fromDay) where

import Data.Time.Calendar (Day, addDays)

fromDay :: Day -> Day
fromDay = addDays daysInGigasecond
  where
    daysInGigasecond = quot (10 ^ (9 :: Int)) (60 * 60 * 24)
