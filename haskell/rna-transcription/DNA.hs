module DNA (toRNA) where

toRNA = map toRNA'

toRNA' c = case c of
    'C' -> 'G'
    'G' -> 'C'
    'A' -> 'U'
    'T' -> 'A'
    _   -> error "Unsupported input"
